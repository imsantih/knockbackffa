package me.imsanti.dev.knockbackffa.listeners;

import me.imsanti.dev.knockbackffa.KnockbackFFA;
import me.imsanti.dev.knockbackffa.managers.ArenaManager;
import me.imsanti.dev.knockbackffa.managers.GameManager;
import me.imsanti.dev.knockbackffa.objects.KnockbackPlayer;
import me.imsanti.dev.knockbackffa.objects.Map;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerListener implements Listener {
    private final KnockbackFFA knockbackFFA;
    private final GameManager gameManager = new GameManager(new ArenaManager());

    public PlayerListener(final KnockbackFFA knockbackFFA) {
        this.knockbackFFA = knockbackFFA;
    }
    @EventHandler
    private void handleJoin(PlayerJoinEvent event) {
        final Map selectedMap = gameManager.getSelectedMap();
        selectedMap.join(event.getPlayer());

    }
}
