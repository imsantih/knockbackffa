package me.imsanti.dev.knockbackffa;

import me.imsanti.dev.knockbackffa.listeners.PlayerListener;
import me.imsanti.dev.knockbackffa.managers.ArenaManager;
import me.imsanti.dev.knockbackffa.managers.GameManager;
import me.imsanti.dev.knockbackffa.objects.Map;
import me.imsanti.dev.knockbackffa.utils.LocationUtils;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public final class KnockbackFFA extends JavaPlugin {

    private Map selectedMap = null;

    private final ArenaManager arenaManager = new ArenaManager();
    private final GameManager gameManager = new GameManager(this);

    @Override
    public void onEnable() {
        arenaManager.loadMap(LocationUtils.getFromXYZ("world", 0, 100, 0), "world", "arena1");
        gameManager.selectRandomMap();
        regsiterEvents();
        // Plugin startup logic

    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }

    private void regsiterEvents() {
        Bukkit.getPluginManager().registerEvents(new PlayerListener(this), this);
    }

    public GameManager getGameManager() {
        return gameManager;
    }

    public ArenaManager getArenaManager() {
        return arenaManager;
    }

    public void setSelectedMap(Map selectedMap) {
        this.selectedMap = selectedMap;
    }

    public Map getSelectedMap() {
        return selectedMap;
    }
}
