package me.imsanti.dev.knockbackffa.managers;

import me.imsanti.dev.knockbackffa.KnockbackFFA;
import me.imsanti.dev.knockbackffa.objects.KnockbackPlayer;
import me.imsanti.dev.knockbackffa.objects.Map;
import org.bukkit.entity.Player;

import java.util.Random;

public class GameManager {

    private final KnockbackFFA knockbackFFA;
    public GameManager(final KnockbackFFA knockbackFFA) {
        this.knockbackFFA = knockbackFFA;
    }

    public void selectRandomMap() {
        if(knockbackFFA.getArenaManager().getLoadedMapsNumber() == 0) return;
        final Random random = new Random();
        final int mapNumber = random.nextInt(knockbackFFA.getArenaManager().getLoadedMapsNumber());
        if(knockbackFFA.getArenaManager().getMap(mapNumber) == null) return;
        knockbackFFA.setSelectedMap(knockbackFFA.getArenaManager().getMap(mapNumber));
    }

    public KnockbackPlayer getPlayer(final Player player) {
        knockbackFFA.getSelectedMap().getPlayers().forEach(mapPlayer -> {
            if(mapPlayer.getUuid().equals(player.getUniqueId())) return;
        });
        return null;
    }

    public Map getSelectedMap() {
        return knockbackFFA.getSelectedMap();
    }
}
