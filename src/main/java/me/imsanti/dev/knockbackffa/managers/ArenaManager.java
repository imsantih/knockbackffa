package me.imsanti.dev.knockbackffa.managers;

import me.imsanti.dev.knockbackffa.objects.Map;
import org.bukkit.Location;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class ArenaManager {


    private final List<Map> loadedMaps = new ArrayList<>();

    public void loadMap(final Location location, final String worldName, final String mapName) {
        loadedMaps.add(new Map(location, worldName, 3, mapName));
    }

    public Map getMap(final String mapName) {
        AtomicReference<Map> map = null;
        loadedMaps.forEach(loadedMap -> {
            if(loadedMap.getMapName().equalsIgnoreCase(mapName)) {
                map.set(loadedMap);
            }
        });
        return map.get();
    }

    public Map getMap(final int listNumber) {
        return loadedMaps.get(listNumber);
    }

    public boolean mapExists(final String mapName) {
        return getMap(mapName) != null;
    }

    public int getLoadedMapsNumber() {
        return loadedMaps.size();
    }
}
