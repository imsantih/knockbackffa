package me.imsanti.dev.knockbackffa.utils;

import org.bukkit.Bukkit;
import org.bukkit.Location;

public class LocationUtils {

    public static Location getFromXYZ(final String worldName, final int x, final int y, final int z) {
        return new Location(Bukkit.getWorld(worldName), x, y, z);
    }

}
