package me.imsanti.dev.knockbackffa.objects;

import me.imsanti.dev.knockbackffa.managers.ArenaManager;
import me.imsanti.dev.knockbackffa.managers.GameManager;

import java.util.UUID;

public class KnockbackPlayer {

    private final GameManager gameManager = new GameManager();

    private final UUID uuid;
    private Map currentMap;
    private int points;
    private int kills;
    private int deaths;

    public KnockbackPlayer(final UUID uuid, final int points, final int kills, final int deaths) {
        this.uuid = uuid;
        this.points = points;
        this.kills = kills;
        this.deaths = deaths;
        this.currentMap = gameManager.getSelectedMap();
    }

    public int getDeaths() {
        return deaths;
    }

    public int getKills() {
        return kills;
    }

    public int getPoints() {
        return points;
    }

    public void add1Death() {
        deaths++;
    }

    public void add1Kill() {
        kills++;
    }

    public UUID getUuid() {
        return uuid;
    }

    public Map getCurrentMap() {
        return currentMap;
    }
}
