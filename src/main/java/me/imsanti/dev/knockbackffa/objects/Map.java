package me.imsanti.dev.knockbackffa.objects;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class Map {

    private final Location spawnLocation;
    private final String worldName;
    private final String mapName;
    private final int blockDespawnTime;

    private final List<KnockbackPlayer> players = new ArrayList<>();

    public Map(final Location spawnLocation, final String worldName, final int blockDespawnTime, final String mapName) {
        this.spawnLocation = spawnLocation;
        this.worldName = worldName;
        this.blockDespawnTime = blockDespawnTime;
        this.mapName = mapName;
    }


    public void join(final Player player) {
        player.teleport(spawnLocation);
        players.add(new KnockbackPlayer(player.getUniqueId(), 0, 0, 0));
    }

    public String getMapName() {
        return mapName;
    }

    public String getWorldName() {
        return worldName;
    }

    public int getBlockDespawnTime() {
        return blockDespawnTime;
    }

    public List<KnockbackPlayer> getPlayers() {
        return players;
    }

    public Location getSpawnLocation() {
        return spawnLocation;
    }
}
